# ****************************************************************************
#  Project:        libguytools
# ****************************************************************************
#  Programmer:     Guy Voncken
#                  Police Grand-Ducale
#                  Service de Police Judiciaire
#                  Section Nouvelles Technologies
# ****************************************************************************
#  Module:         Log utility
# ****************************************************************************

# Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,
# 2018, 2019
# Guy Voncken
#
# This file is part of libguytools.
#
# libguytools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# libguytools is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with libguytools. If not, see <http://www.gnu.org/licenses/>.

TEMPLATE = lib
DESTDIR  = ./lib
TARGET  = guytools

include( ./libguytools_version.pro.inc )

# The following line allows the software to see its own version
# (just for logging purpose). Many slashes and quotes... look at
# result of it during compilation
DEFINES += "LIBGUYTOOLS_VERSION=\\\""$$VERSION"\\\""

# Use the standard Qt configuration but remove the GUI part, as we do not need it.
CONFIG  += qt
QT      -= gui
CONFIG  += c++14

CONFIG  += warn_on thread release
DEFINES += TOCFG_COMPILE_FOR_USE_WITHOUT_TOOLBOX

DEPENDPATH  += ./include
INCLUDEPATH += ./include

QMAKE_CXXFLAGS *= $(shell dpkg-buildflags --get CXXFLAGS)
QMAKE_LFLAGS   *= $(shell dpkg-buildflags --get LDFLAGS)

QMAKE_CXXFLAGS_WARN_ON += -Wno-strict-aliasing   # Switch off warning "dereferencing type-punned pointer will break strict-aliasing rules"
QMAKE_CXXFLAGS_WARN_ON += -fmessage-length=0     # Tell g++ not to split messages into different lines
QMAKE_CXXFLAGS_RELEASE += -O3

SOURCES += toollog.cpp
SOURCES += toolerror.cpp
SOURCES += toolsysinfo.cpp
SOURCES += toolsignal.cpp
SOURCES += toolcfg.cpp


