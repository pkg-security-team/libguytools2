// ****************************************************************************
//  Project:        libguytools
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         Unique IDs for tools and applications
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,
// 2018, 2019
// Guy Voncken
//
// This file is part of libguytools.
//
// libguytools is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libguytools is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libguytools. If not, see <http://www.gnu.org/licenses/>.


#ifndef __TOOLGLOBALID_H__
#define __TOOLGLOBALID_H__


/* ------------------------------- */
/*           Constants             */
/* ------------------------------- */

/* ------------------------------- */
/*            Base id's            */
/* ------------------------------- */

const unsigned int ID_BASE                    =       0;
const unsigned int ID_OFFSET_PROJECT          =10000000;   /* 100 projects                 */
const unsigned int ID_OFFSET_SUBPROJECT       =  100000;   /* 100 subprojects              */
const unsigned int ID_OFFSET_SUBSUBPROJECT    =    1000;   /* 1st possibility -- 100 subsubprojects (=100 C-files with max. 1000 IDs) */
const unsigned int ID_OFFSET_SUBSUBSUBPROJECT =     100;   /* 2nd possibility -- 100 subsubprojects (=100 subdir's)                                                               */
                                                           /*                    each of the 100 subsubprojects has max. 10 subsubsubprojects (=10 C-files with max. 100 IDs) */

#define CREATE_BASE_ID(ProjectName, x) \
   const unsigned int ID_BASE_ ## ProjectName = x;

#define CREATE_IDS(ModulName, x)             \
   const unsigned int ERROR_BASE_ ## ModulName = x;   \
   const unsigned int MEMID_BASE_ ## ModulName = x;

/* ========================== TOOLS ================================ */

CREATE_BASE_ID (TOOLS, ID_BASE + 0*ID_OFFSET_PROJECT)
CREATE_IDS (TOOL_CFG         , ID_BASE_TOOLS +  0*ID_OFFSET_SUBPROJECT)
CREATE_IDS (TOOL_ERROR       , ID_BASE_TOOLS +  1*ID_OFFSET_SUBPROJECT)
CREATE_IDS (TOOL_LOG         , ID_BASE_TOOLS +  2*ID_OFFSET_SUBPROJECT)
CREATE_IDS (TOOL_SIGNAL      , ID_BASE_TOOLS +  3*ID_OFFSET_SUBPROJECT)
CREATE_IDS (TOOL_SYSINFO     , ID_BASE_TOOLS +  4*ID_OFFSET_SUBPROJECT)


/* ========================= GUYMAGER ============================== */

CREATE_BASE_ID (GUYMAGER, ID_BASE + 1*ID_OFFSET_PROJECT)
CREATE_IDS (GUYMAGER_MAIN        , ID_BASE_GUYMAGER + 0*ID_OFFSET_SUBPROJECT)

#endif

