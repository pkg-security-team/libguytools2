// ****************************************************************************
//  Project:        libguytools
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         Signal handler with backtrace
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,
// 2018, 2019
// Guy Voncken
//
// This file is part of libguytools.
//
// libguytools is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libguytools is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libguytools. If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLSIGNAL_H__
#define __TOOLSIGNAL_H__


// Description of module ToolSignal
// --------------------------------
// This module does all the standard signal handling work for an application. When catching a standard
// signal (Kill, Hup, Break...) it calls the function that has been indicated in param pFnLog.
// If ever a segment violation occurs, a stack trace is done in order to log the function call sequence
// before exiting the program.
//
// All logging is done via the function indicated in param pFnLog. If that one is NULL, the module writes
// its log outputs to stdout. Its first parameter (Error) indicates whether a normal log output (info) or
// whether an error log entry should be done.
//
// It might be important to know for the application, that ToolSignalInit starts a new thread. The function
// indicated in param pFnSig is called from that thread, if pFnSig is not NULL. The ID of the new thread is
// written to where pThreadId points, if pThreadId is not NULL.

typedef void (* t_pToolSignalLogFn  )(bool Error, unsigned long long ThreadId, const char *pFileName, const char *pFunction, int LineNr, const char *pFormat, va_list pArguments);
typedef void (* t_pToolSignalHandler)(int Signal);

int ToolSignalInit   (t_pToolSignalLogFn pFnLog, t_pToolSignalHandler pFnSig, unsigned long long *pThreadId);
int ToolSignalDeInit (void);


// class t_ToolSignal: public QThread
// {
//    public:
//       t_ToolSignal (int Err, int *pThreadId);
//      ~t_ToolSignal();
//       void run();
//       static void BacktraceSignalHandler (int signalHandled, siginfo_t *signalInfo, void *secret);
//
//       static APIRET SetLogFn           (t_pToolSignalLogFn   pFn);
//       static APIRET SetSignalHandlerFn (t_pToolSignalHandler pFn);
// };
// typedef t_ToolSignal *t_pToolSignal;


// -------------------------------
//           Error codes
// -------------------------------

enum
{
    ERROR_TOOLSIGNAL_ALREADY_INITIALISED = ERROR_BASE_TOOL_SIGNAL + 1,
    ERROR_TOOLSIGNAL_NOT_INITIALISED,
    ERROR_TOOLSIGNAL_STARTTHREAD_FAILED
};

#endif
