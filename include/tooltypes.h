// ****************************************************************************
//  Project:        libguytools
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         Type definitions
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,
// 2018, 2019
// Guy Voncken
//
// This file is part of libguytools.
//
// libguytools is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libguytools is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libguytools. If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLTYPES_H__
#define __TOOLTYPES_H__

#ifndef NO_ERROR
   #define NO_ERROR 0
#endif

// Remember, char, signed char and unsigned char are 3 different types
// as stated by ANSI-C, but there are only two representations !!!

typedef char             t_char;
typedef char *           t_pchar;
typedef const char *     t_pcchar;

typedef signed char      t_schar;
typedef signed char *    t_pschar;
typedef unsigned char    t_uchar;
typedef unsigned char *  t_puchar;

typedef short            t_short;
typedef short *          t_pshort;
typedef unsigned short   t_ushort;
typedef unsigned short * t_pushort;

typedef int              t_int;
typedef int *            t_pint;
typedef unsigned int     t_uint;
typedef unsigned int *   t_puint;

typedef void             t_void;
typedef void *           t_pvoid;

typedef float            t_float;
typedef float *          t_pfloat;
typedef double           t_double;
typedef double *         t_pdouble;

#endif

