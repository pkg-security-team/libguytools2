// ****************************************************************************
//  Project:        libguytools
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         Different useful constants
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,
// 2018, 2019
// Guy Voncken
//
// This file is part of libguytools.
//
// libguytools is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libguytools is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libguytools. If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLCONSTANTS_H__
#define __TOOLCONSTANTS_H__

/* --------------------------------------------------------------------------------------------- */
/*  Attention                                                                                    */
/*  This files only defines general, never-changing constants. Do not add application dependant  */
/*  definitions, such as string lengths, stack sizes etc.                                        */
/* --------------------------------------------------------------------------------------------- */

/* ---------------- */
/*  Time constants  */
/* ---------------- */

#define MSEC_PER_SECOND         1000
#define USEC_PER_MSEC           1000

#define SECONDS_PER_DAY        86400
#define SECONDS_PER_HOUR        3600
#define SECONDS_PER_MINUTE        60

#define MSEC_PER_DAY        (MS_PER_S * S_PER_DAY   )
#define MSEC_PER_HOUR       (MS_PER_S * S_PER_HOUR  )
#define MSEC_PER_MINUTE     (MS_PER_S * S_PER_MINUTE)

#define DAYS_PER_WEEK              7
#define SECONDS_PER_WEEK    (DAYS_PER_WEEK*SECONDS_PER_DAY)
#define DAYS_PER_YEAR            365                              // Attention, this is of
#define SECONDS_PER_YEAR    (SECONDS_PER_DAY * DAYS_PER_YEAR)     // course just an estimation
#define MONTHS_PER_YEAR           12

/* ---------------------- */
/*  Weights and measures  */
/* ---------------------- */

static const double LBS_PER_KG   = 2.204623;
static const double MM_PER_INCH  = 25.4;

#define UM_PER_M   1000000
#define UM_PER_MM     1000
#define MM_PER_M      1000

/* ---------------------- */
/*       Mathematics      */
/* ---------------------- */

static const double PI = 3.141592654;

/* ---------------------- */
/*     Computer stuff     */
/* ---------------------- */

#define BYTES_PER_KILOBYTE      1024
#define BYTES_PER_MEGABYTE   1048576

#define CLIB_YEAROFFSET         1900    // Fixed year offset for struct tm (by ANSI)

#define TCPIP_ADDRESS_LEN         15    // Address format: XXX.XXX.XXX.XXX -> 15  -- do not forget to add +1 if using C strings

#ifndef TRUE
   #define TRUE  1
   #define FALSE 0
#endif

/* ------------------ */
/*  ASCII characters  */
/* ------------------ */

static const char ASCII_ESC        =  27;
static const char ASCII_BS         = 0x08;
static const char ASCII_LF         = 0x0A;
static const char ASCII_CR         = 0x0D;
static const char ASCII_ENTER      = ASCII_CR;
static const char ASCII_EOF        = 0x1A;

#endif

