// ****************************************************************************
//  Project:        libguytools
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         System information
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,
// 2018, 2019
// Guy Voncken
//
// This file is part of libguytools.
//
// libguytools is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libguytools is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libguytools. If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLSYSINFO_H__
#define __TOOLSYSINFO_H__

// ------------------------------
//    Constants and Structures
// ------------------------------

const int TOOLSYSINFO_MACADDRLEN_VAL = 6;
const int TOOLSYSINFO_MACADDRLEN_STR = 2*TOOLSYSINFO_MACADDRLEN_VAL;

typedef struct
{
   unsigned char AddrVal [TOOLSYSINFO_MACADDRLEN_VAL  ];
   char          AddrStr [TOOLSYSINFO_MACADDRLEN_STR+1];
} t_ToolSysInfoMacAddr, *t_pToolSysInfoMacAddr;

// ------------------------------
//          Functions
// ------------------------------

APIRET ToolSysInfoGetMacAddr (t_pToolSysInfoMacAddr pMacAddr);
APIRET ToolSysInfoUname      (QString &Uname);

APIRET ToolSysInfoInit   (void);
APIRET ToolSysInfoDeInit (void);


// ------------------------------
//          Error codes
// ------------------------------

enum
{
   TOOLSYSINFO_ALREADY_INITIALISED = ERROR_BASE_TOOL_SYSINFO + 1,
   TOOLSYSINFO_ERROR_SOCKET,
   TOOLSYSINFO_ERROR_NO_ADDR,
   TOOLSYSINFO_ERROR_UNAME
};

#endif

