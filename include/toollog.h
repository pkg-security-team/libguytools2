// ****************************************************************************
//  Project:        libguytools
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         Log utility
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017,
// 2018, 2019
// Guy Voncken
//
// This file is part of libguytools.
//
// libguytools is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// libguytools is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with libguytools. If not, see <http://www.gnu.org/licenses/>.

#ifndef __LOG_H__
#define __LOG_H__

class t_Log
{
   public:
       typedef enum
       {
          Debug=0,
          Info,
          Error,

          Entries // must always be the last element in the list
       } t_Level;


     t_Log (const char* pFileName, APIRET &rc);
    ~t_Log (void);

     static bool IsInitialised (void);
     static void GetLibGuyToolsVersion   (const char **pVersion);

     static void vEntry   (t_Level Level, const char *pFileName, const char *pFunctionName, int LineNr, const char *pFormat, va_list pArguments);
     static void  Entry   (t_Level Level, const char *pFileName, const char *pFunctionName, int LineNr, const char *pFormat, ...) __attribute__ ((format (printf, 5, 6)));
     static void vEntryDebug             (const char *pFileName, const char *pFunctionName, int LineNr, const char *pFormat, va_list pArguments);
     static void vEntryInfo              (const char *pFileName, const char *pFunctionName, int LineNr, const char *pFormat, va_list pArguments);
     static void vEntryError             (const char *pFileName, const char *pFunctionName, int LineNr, const char *pFormat, va_list pArguments);

     #define LOG_DEBUG(...)  t_Log::Entry (t_Log::Debug, __FFL__, __VA_ARGS__);
     #define LOG_INFO(...)   t_Log::Entry (t_Log::Info , __FFL__, __VA_ARGS__);
     #define LOG_ERROR(...)  t_Log::Entry (t_Log::Error, __FFL__, __VA_ARGS__);

   // Possible error codes
   // --------------------
      static const APIRET TOOLLOG_ERROR_INITIALISED_TWICE = ERROR_BASE_TOOL_LOG + 1;
      static const APIRET TOOLLOG_ERROR_HOSTNAME_TOO_LONG = ERROR_BASE_TOOL_LOG + 2;
};

#endif

